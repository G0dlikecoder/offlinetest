import { app } from "electron";
// import { autoUpdater, AppUpdater } from "electron-updater";
// Quit when all windows are closed.
// let curWindow;
// //Basic flags
// autoUpdater.autoDownload = false;
// autoUpdater.autoInstallOnAppQuit = true;
//   console.log('Update available.')
//   app.whenReady().then(() => {
//   autoUpdater.checkForUpdates();
//   curWindow.showMessage(`Checking for updates. Current version ${app.getVersion()}`);
// });

// /*New Update Available*/
// autoUpdater.on("update-available", (info) => {
//   console.log('Update available.')
//   curWindow.showMessage(`Update available. Current version ${app.getVersion()}`);
//   let pth = autoUpdater.downloadUpdate();
//   curWindow.showMessage(pth);
// });

// autoUpdater.on("update-not-available", (info) => {
//   console.log('No Update available.')
//   curWindow.showMessage(`No update available. Current version ${app.getVersion()}`);
// });

// /*Download Completion Message*/
// autoUpdater.on("update-downloaded", (info) => {
//   console.log('Update downlouded.')
//   curWindow.showMessage(`Update downloaded. Current version ${app.getVersion()}`);
// });

// autoUpdater.on("error", (info) => {
//   curWindow.showMessage(info);
// });



const exec = require("child_process").exec;
console.log('backend initializing');
const child = exec("cd resources/app/api & node src/index.js > app.log", (e, st, std) => {
// const child = exec("cd api & node src/index.js > app.log", (e, st, std) => {
  console.log("e", std);
});
console.log('backend started');
app.on("window-all-closed", function () {
  // On macOS it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== "darwin") {
    child.kill();
  };
  // kill server
});
require("./mainWindow");
